import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import InitialScreen from '../screens/Authentication/InitialScreen';
import LoginScreen from '../screens/Authentication/LoginScreen';
import RegisterScreen from '../screens/Authentication/RegisterScreen';

const Stack = createStackNavigator();
export default function AuthStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={InitialScreen} />
      <Stack.Screen name="Notifications" component={LoginScreen} />
      <Stack.Screen name="Profile" component={RegisterScreen} />
    </Stack.Navigator>
  );
}
